package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.UserBooks;

@Repository
public interface UserBooksRepository extends JpaRepository<UserBooks, Integer> {
	public List<UserBooks> findAll();
	public Optional<UserBooks> findById(int id);
	public void deleteById(int id);
	public List<UserBooks> findByUserId(int id);
	public List<UserBooks> findByBookId(int id);
	public UserBooks deleteByUserId(int id);
	public UserBooks deleteByBookId(int id);
}
