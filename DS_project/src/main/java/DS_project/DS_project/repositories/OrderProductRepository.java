package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.OrderProduct;

@Repository
public interface OrderProductRepository extends JpaRepository<OrderProduct, Integer>{
	public List<OrderProduct> findAll();
	public Optional<OrderProduct> findById(int id);
	public List<OrderProduct> findByOrderId(int id);
	public List<OrderProduct> findByProductId(int id);
	public OrderProduct deleteByOrderId(int id);
	public OrderProduct deleteByProductId(int id);
	public void deleteById(int id);
}
