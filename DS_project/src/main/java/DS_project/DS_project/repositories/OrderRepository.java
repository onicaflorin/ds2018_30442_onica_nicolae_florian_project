package DS_project.DS_project.repositories;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>{
	public List<Order> findAll();
	public Optional<Order> findById(int id);
	public List<Order> findByUserId(int id);
	public List<Order> findByDate(Date date);
	public void deleteById(int id);
}