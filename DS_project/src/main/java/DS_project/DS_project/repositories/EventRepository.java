package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
	public List<Event> findAll();
	public List<Event> findAllByOrderByTicketsDesc();
	public Optional<Event> findById(int id);
	public void deleteById(int id);
}
