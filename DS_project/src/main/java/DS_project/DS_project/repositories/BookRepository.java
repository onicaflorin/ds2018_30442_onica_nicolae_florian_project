package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
	public List<Book> findAll();
	public Optional<Book> findById(int id);
	public List<Book> findByAuthor(String author);
	public void deleteById(int id);
}