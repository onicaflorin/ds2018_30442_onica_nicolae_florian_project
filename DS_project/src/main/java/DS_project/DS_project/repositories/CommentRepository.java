package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
	public List<Comment> findAll();
	public Optional<Comment> findById(int id);
	public List<Comment> findByBookId(int id);
	public void deleteById(int id);
}	
