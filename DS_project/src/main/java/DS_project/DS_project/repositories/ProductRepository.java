package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{
	public List<Product> findAll();
	public Optional<Product> findById(int id);
	public List<Product> findByPrice(int price);
	public List<Product> findByStock(int stock);
	public Product findByName(String name);
	public List<Product> findByType(String type);
	public void deleteById(int id);
}