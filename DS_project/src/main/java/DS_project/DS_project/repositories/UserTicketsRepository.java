package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.UserTickets;

@Repository
public interface UserTicketsRepository extends JpaRepository<UserTickets, Integer> {
	public List<UserTickets> findAll();
	public Optional<UserTickets> findById(int id);
	public void deleteById(int id);
	public List<UserTickets> findByUserId(int id);
	public List<UserTickets> findByEventId(int id);
	public UserTickets deleteByUserId(int id);
	public UserTickets deleteByEventId(int id);
}
