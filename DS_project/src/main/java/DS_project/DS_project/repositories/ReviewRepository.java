package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer>{
	public List<Review> findAll();
	public Optional<Review> findById(int id);
	public List<Review> findByBookId(int id);
	public List<Review> findByFlagged(Boolean flag);
	public void deleteById(int id);
}