package DS_project.DS_project.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import DS_project.DS_project.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	public List<User> findAll();
	public Optional<User> findById(int id);
	public User findByUsernameAndPassword(String username, String password);
	public User findByFirstNameAndLastName(String firstName, String lastName);
}
