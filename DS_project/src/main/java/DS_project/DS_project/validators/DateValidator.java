package DS_project.DS_project.validators;

import java.time.LocalDate;

import DS_project.DS_project.entities.Order;

public class DateValidator implements Validator<Order>{
  public void validate(Order t) {
	if(t.getDate().after(java.sql.Date.valueOf(LocalDate.now()))) {
   	  throw new IllegalArgumentException("Order date can not be in the future");
	}
  }
}
