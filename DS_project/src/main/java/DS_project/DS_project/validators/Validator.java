package DS_project.DS_project.validators;

public interface Validator<T> {
  public void validate(T t);
}
