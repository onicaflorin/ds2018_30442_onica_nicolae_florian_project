package DS_project.DS_project.validators;

import DS_project.DS_project.entities.Product;

public class StockValidator implements Validator<Product>{
  public void validate(Product t) {
	if(t.getStock() < 0) {
		throw new IllegalArgumentException("The product stock can't be negative!");
	}
  }
}
