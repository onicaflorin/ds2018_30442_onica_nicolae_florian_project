package DS_project.DS_project.validators;

import DS_project.DS_project.entities.OrderProduct;

public class QuantityValidator implements Validator<OrderProduct>{
  public void validate(OrderProduct t) {
	if(t.getQuantity() < 0) {
 	  throw new IllegalArgumentException("The product quantity can't be negative!");
	}
  }
}
