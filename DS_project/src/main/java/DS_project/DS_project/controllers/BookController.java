package DS_project.DS_project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import DS_project.DS_project.entities.Book;
import DS_project.DS_project.entities.User;
import DS_project.DS_project.services.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	@Autowired
	private BookService bookService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Book> getAllBooks() {
		return bookService.findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Book getUserById(@PathVariable("id") int id) {
		return bookService.getById(id);
	}
	
	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public List<Book> getBooksByAuthor(@RequestBody String author) {
		return bookService.findByAuthor(author);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Book> insertBook(@RequestBody Book book) {
		//if(user.getType() == 0) {
			return ResponseEntity.status(HttpStatus.OK).body(bookService.createBook(book));
		//}
		//return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Book updateBook(@RequestBody Book book, @PathVariable("id") int id) {
		Book b = bookService.getById(id);
		b = book;
		return bookService.getBookRepository().save(b);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public HttpStatus deleteBook(@RequestBody Book book) {
		//if(user.getType() == 0) {
			bookService.delete(book);
			return HttpStatus.OK;
		//}
		//return HttpStatus.FORBIDDEN;
	}

}
