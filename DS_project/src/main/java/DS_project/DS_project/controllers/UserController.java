package DS_project.DS_project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import DS_project.DS_project.entities.User;
import DS_project.DS_project.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userService.getAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public User insertUser(@RequestBody User user) {
		return userService.createUser(user);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteUser(@RequestBody User user) {
		userService.delete(user);
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public User login(@RequestBody String user, String pass) {
		return userService.getByLogin("user", "pass");
	}
}
