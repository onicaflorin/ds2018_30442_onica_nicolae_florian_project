package DS_project.DS_project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import DS_project.DS_project.entities.Event;
import DS_project.DS_project.services.EventService;

@RestController
@RequestMapping("/event")
public class EventController {
	@Autowired
	private EventService eventService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Event> getAllEvents() {
		return eventService.getAll();
	}
	
	@RequestMapping(value = "/most-tickets/{number}", method = RequestMethod.GET)
	public List<Event> getEventsWithMostTickets(@PathVariable("number") int number) {
		return eventService.findEventsWithMostTicketsAvailable(number);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Event getUserById(@PathVariable("id") int id) {
		return eventService.getById(id);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public Event insertEvent(@RequestBody Event event) {
		return eventService.createEvent(event);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Event updateEvent(@RequestBody Event event, @PathVariable("id") int id) {
		Event b = eventService.getById(id);
		b = event;
		return eventService.getEventRepository().save(b);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteEvent(@RequestBody Event event) {
		eventService.delete(event);
	}
}
