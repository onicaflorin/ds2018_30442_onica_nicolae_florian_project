package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.User;
import DS_project.DS_project.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	public User createUser(User c) {
		return userRepository.save(c);
	}
	
	public List<User> getAll() {
		return userRepository.findAll();
	}
	
	public void delete(User s) {
		userRepository.delete(s);
	}
	
	public User getById(int id) {
		return userRepository.findById(id).get();
	}
	
	public User getByName(String fname, String lname) {
		return userRepository.findByFirstNameAndLastName(fname, lname);
	}
	
	public User getByLogin(String user, String pass) {
		return userRepository.findByUsernameAndPassword(user, pass);
	}
}
