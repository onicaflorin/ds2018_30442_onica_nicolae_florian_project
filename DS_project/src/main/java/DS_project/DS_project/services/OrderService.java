package DS_project.DS_project.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.Order;
import DS_project.DS_project.repositories.OrderRepository;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	
	public Order makeOrder(Order o) {
		return orderRepository.save(o);
	}
	
	public List<Order> getAll() {
		return orderRepository.findAll();
	}
	
	public void delete(Order o) {
		orderRepository.delete(o);
	}
	
	public void deleteById(int id) {
		orderRepository.deleteById(id);
	}
	
	public Order getById(int id) {
		return orderRepository.findById(id).get();
	}
	
	public List<Order> getByUserId(int id) {
		return orderRepository.findByUserId(id);
	}
	
	public List<Order> getByDate(Date date) {
		return orderRepository.findByDate(date);
	}
}
