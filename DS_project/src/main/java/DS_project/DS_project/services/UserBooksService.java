package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.UserBooks;
import DS_project.DS_project.repositories.UserBooksRepository;

@Service
public class UserBooksService {
	@Autowired
	private UserBooksRepository userBooksRepository;
	
	public UserBooks createUserBooks(UserBooks c) {
		return userBooksRepository.save(c);
	}
	
	public List<UserBooks> getAll() {
		return userBooksRepository.findAll();
	}
	
	public void delete(UserBooks s) {
		userBooksRepository.delete(s);
	}
	
	public UserBooks getById(int id) {
		return userBooksRepository.findById(id).get();
	}
	
	public List<UserBooks> getByUserId(int id) {
		return userBooksRepository.findByUserId(id);
	}
	
	public List<UserBooks> getByBookId(int id) {
		return userBooksRepository.findByBookId(id);
	}
	
	public void deleteByUserId(int id) {
		userBooksRepository.deleteByUserId(id);
	}
	
	public void deleteByBookId(int id) {
		userBooksRepository.deleteByBookId(id);
	}
}
