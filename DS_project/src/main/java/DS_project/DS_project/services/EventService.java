package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.Event;
import DS_project.DS_project.repositories.EventRepository;

@Service
public class EventService {
	@Autowired
	private EventRepository eventRepository;
	
	public Event createEvent(Event c) {
		return eventRepository.save(c);
	}
	
	public List<Event> getAll() {
		return eventRepository.findAll();
	}
	
	public List<Event> findEventsWithMostTicketsAvailable(int number) {
		return eventRepository.findAllByOrderByTicketsDesc().subList(0, number);
	}
	
	public void delete(Event s) {
		eventRepository.delete(s);
	}
	
	public Event getById(int id) {
		return eventRepository.findById(id).get();
	}

	public EventRepository getEventRepository() {
		return eventRepository;
	}

	public void setEventRepository(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}
}
