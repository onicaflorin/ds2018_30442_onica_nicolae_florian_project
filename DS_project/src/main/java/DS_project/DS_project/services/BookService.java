package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.Book;
import DS_project.DS_project.repositories.BookRepository;

@Service
public class BookService {
	@Autowired
	private BookRepository bookRepository;
	
	public Book createBook(Book c) {
		return bookRepository.save(c);
	}
	
	public List<Book> findAll() {
		return bookRepository.findAll();
	}
	
	public void delete(Book s) {
		bookRepository.delete(s);
	}
	
	public List<Book> findByAuthor(String author) {
		return bookRepository.findByAuthor(author);
	}
	
	public Book getById(int id) {
		return bookRepository.findById(id).get();
	}
	
	public void deleteById(int id) {
		bookRepository.deleteById(id);
	}

	public BookRepository getBookRepository() {
		return bookRepository;
	}

	public void setBookRepository(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}
}
