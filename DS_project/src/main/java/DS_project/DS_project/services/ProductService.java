package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.Product;
import DS_project.DS_project.repositories.ProductRepository;
import DS_project.DS_project.validators.PriceValidator;
import DS_project.DS_project.validators.StockValidator;

@Service
public class ProductService {
	@Autowired
	private ProductRepository productRepository;

	public Product makeProduct(Product p) {
		PriceValidator pv = new PriceValidator();
		StockValidator sv = new StockValidator();
		pv.validate(p);
		sv.validate(p);
		return productRepository.save(p);
	}

	public List<Product> getAll() {
		return productRepository.findAll();
	}

	public void delete(Product p) {
		productRepository.delete(p);
	}

	public void deleteById(int id) {
		productRepository.deleteById(id);
	}

	public Product getById(int id) {
		return productRepository.findById(id).get();
	}

	public List<Product> getByPrice(int productRepositoryice) {
		return productRepository.findByPrice(productRepositoryice);
	}

	public List<Product> getByStock(int stock) {
		return productRepository.findByStock(stock);
	}

	public Product getByName(String name) {
		return productRepository.findByName(name);
	}

	public List<Product> getByType(String type) {
		return productRepository.findByType(type);
	}
}