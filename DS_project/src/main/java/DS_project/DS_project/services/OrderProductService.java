package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.OrderProduct;
import DS_project.DS_project.repositories.OrderProductRepository;
import DS_project.DS_project.validators.QuantityValidator;

@Service
public class OrderProductService {
	@Autowired
	private OrderProductRepository orderProductRepository;
	
	public OrderProduct makeOrderProduct(OrderProduct op) {
		QuantityValidator qv = new QuantityValidator();
		qv.validate(op);
		return orderProductRepository.save(op);
	}
	
	public List<OrderProduct> getAll() {
		return orderProductRepository.findAll();
	}
	
	public void delete(OrderProduct op) {
		orderProductRepository.delete(op);
	}
	
	public void deleteById(int id) {
		orderProductRepository.deleteById(id);
	}
	
	public OrderProduct getById(int id) {
		return orderProductRepository.findById(id).get();
	}
	
	public List<OrderProduct> getByProductId(int id) {
		return orderProductRepository.findByProductId(id);
	}
	
	public List<OrderProduct> getByOrderId(int id) {
		return orderProductRepository.findByOrderId(id);
	}
	
	public void deleteByProductId(int id) {
		orderProductRepository.deleteByProductId(id);
	}
	
	public void deleteByOrderId(int id) {
		orderProductRepository.deleteByOrderId(id);
	}
}