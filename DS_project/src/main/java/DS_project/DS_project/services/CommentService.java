package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.Comment;
import DS_project.DS_project.repositories.CommentRepository;

@Service
public class CommentService {
	@Autowired
	private CommentRepository commentRepository;
	
	public Comment createComment(Comment c) {
		return commentRepository.save(c);
	}
	
	public List<Comment> getAll() {
		return commentRepository.findAll();
	}
	
	public List<Comment> findByBookId(int id) {
		return commentRepository.findByBookId(id);
	}
	
	public void delete(Comment s) {
		commentRepository.delete(s);
	}
	
	public Comment getById(int id) {
		return commentRepository.findById(id).get();
	}
}
