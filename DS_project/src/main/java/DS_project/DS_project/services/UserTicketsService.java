package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.UserTickets;
import DS_project.DS_project.repositories.UserTicketsRepository;

@Service
public class UserTicketsService {
	@Autowired
	private UserTicketsRepository userTicketsRepository;
	
	public UserTickets createUserTickets(UserTickets c) {
		return userTicketsRepository.save(c);
	}
	
	public List<UserTickets> getAll() {
		return userTicketsRepository.findAll();
	}
	
	public void delete(UserTickets s) {
		userTicketsRepository.delete(s);
	}
	
	public UserTickets getById(int id) {
		return userTicketsRepository.findById(id).get();
	}
	
	public List<UserTickets> getByUserId(int id) {
		return userTicketsRepository.findByUserId(id);
	}
	
	public List<UserTickets> getByEventId(int id) {
		return userTicketsRepository.findByEventId(id);
	}
	
	public void deleteByUserId(int id) {
		userTicketsRepository.deleteByUserId(id);
	}
	
	public void deleteByOrderId(int id) {
		userTicketsRepository.deleteByEventId(id);
	}
}
