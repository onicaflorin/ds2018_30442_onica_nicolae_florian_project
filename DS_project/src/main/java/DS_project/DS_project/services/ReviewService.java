package DS_project.DS_project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DS_project.DS_project.entities.Review;
import DS_project.DS_project.repositories.ReviewRepository;

@Service
public class ReviewService {
	@Autowired
	private ReviewRepository reviewRepository;
	
	public Review makeReview(Review r) {
		return reviewRepository.save(r);
	}
	
	public List<Review> getAll() {
		return reviewRepository.findAll();
	}
	
	public void delete(Review r) {
		reviewRepository.delete(r);
	}
	
	public void deleteById(int id) {
		reviewRepository.deleteById(id);
	}
	
	public Review getById(int id) {
		return reviewRepository.findById(id).get();
	}
	
	public List<Review> getByBookId(int id) {
		return reviewRepository.findByBookId(id);
	}
	
	public List<Review> getByFlagged(Boolean flag) {
		return reviewRepository.findByFlagged(flag);
	}
}