package DS_project.DS_project.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "books")
public class Book implements java.io.Serializable {
	public Book(int id, String title, String author, String isbn, String description) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.isbn = isbn;
		this.description = description;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5625457114121909464L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "title", unique = true)
	private String title;
	
	@Column(name = "author", unique = true)
	private String author;
	
	@Column(name = "isbn", unique = true)
	private String isbn;
	
	@Lob
	@Column(name = "description")
	private String description;
	
	public Book() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
